Task API
========

The Task API is a Spring Boot application that provides CRUD (Create, Read, Update, Delete) operations for managing
tasks. The application uses Docker Compose to simplify the deployment process.

Prerequisites
-------------

Make sure you have the following software installed on your machine:

- Docker
- Docker Compose

Getting Started
---------------

1. Clone the repository:


- `git clone git@gitlab.com:BlueRewind/task-crud-api.git`

    - Navigate to the project directory:

    - `cd task-crud-api`


- Build and run the application using Docker Compose:

1. `docker-compose build`

    - This command will download dependencies and build the Docker images
2. `docker-compose up -d`
    - This will start the application stack in detached mode.

3. Access the application:

    - The Task API will be available at http://localhost:8080.
        - username: `username`
        - password: `password`
    - PgAdmin (database management tool) will be available at http://localhost:5050.
        - email: `user@domain.com`
        - password: `password`
        - You can change these in the application.yml

Usage
-----

- The API provides RESTful endpoints for managing tasks. You can explore the API using tools
  like [Postman](https://www.postman.com/) or [curl](https://curl.se/).

- Documentation for the API can be found at http://localhost:8080/swagger-ui/index.html.

Cleanup
-------

To stop and remove the Docker containers, use the following command:

bash

`docker-compose down`

* * * * *

