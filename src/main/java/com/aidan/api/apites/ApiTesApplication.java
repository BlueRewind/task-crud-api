package com.aidan.api.apites;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ConfigurationPropertiesScan
@Profile("dev")
public class ApiTesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiTesApplication.class, args);
    }
}
