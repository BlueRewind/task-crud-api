package com.aidan.api.apites.tasks.repository;

import com.aidan.api.apites.tasks.model.TaskCategory;
import com.aidan.api.apites.tasks.model.TaskStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TaskRepository extends JpaRepository<TaskEntity, UUID> {
    Page<TaskEntity> findAll(Pageable pageable);

    Optional<Page<TaskEntity>> findByName(String name, Pageable pageable);

    @Query("SELECT t FROM TaskEntity t WHERE t.status = :status")
    Page<TaskEntity> getByStatus(TaskStatus status, Pageable pageable);

    @Query("SELECT t FROM TaskEntity t WHERE t.category = :category")
    Page<TaskEntity> getByCategory(TaskCategory category, Pageable pageable);

    @Transactional
    @Modifying
    @Query("update TaskEntity t set t.status = ?2 where t.id = ?1")
    void updateStatusByStatus(UUID taskId, String status);
}
