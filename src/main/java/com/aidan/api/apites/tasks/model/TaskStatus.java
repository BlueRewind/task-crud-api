package com.aidan.api.apites.tasks.model;

public enum TaskStatus {
    CREATED, IN_PROGRESS, PAUSED, FINISHED, ON_HOLD
}
