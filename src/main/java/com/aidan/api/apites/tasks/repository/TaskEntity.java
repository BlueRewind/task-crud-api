package com.aidan.api.apites.tasks.repository;

import com.aidan.api.apites.tasks.model.TaskCategory;
import com.aidan.api.apites.tasks.model.TaskStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;


@Entity
@Builder
@Table(name = "tasks")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskEntity {
    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @Enumerated(EnumType.STRING)
    private TaskCategory category;
    private int priority;
    private LocalDateTime dueDate;
    private LocalDateTime creationDate;
}
