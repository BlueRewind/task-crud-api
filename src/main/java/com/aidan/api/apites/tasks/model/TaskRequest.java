package com.aidan.api.apites.tasks.model;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDateTime;
import java.util.UUID;

@Validated
public record TaskRequest(
        @NotNull UUID id,
        @NotBlank String name,
        @NotNull TaskStatus status,
        @NotNull TaskCategory category,
        @Min(1)
        @Max(5)
        int priority,
        LocalDateTime dueDate
) {
}
