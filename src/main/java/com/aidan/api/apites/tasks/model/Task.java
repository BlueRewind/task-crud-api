package com.aidan.api.apites.tasks.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    private UUID id;
    private String name;
    private TaskStatus status;
    private TaskCategory category;
    private int priority;
    private LocalDateTime dueDate;
    private LocalDateTime creationDate;
}
