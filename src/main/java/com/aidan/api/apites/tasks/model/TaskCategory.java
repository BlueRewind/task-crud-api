package com.aidan.api.apites.tasks.model;

public enum TaskCategory {
    PERSONAL, WORK, EDUCATION, FAMILY, SOCIAL, HEALTH, FINANCIAL, TRAVEL, ENTERTAINMENT
}
