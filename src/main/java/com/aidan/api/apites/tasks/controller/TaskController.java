package com.aidan.api.apites.tasks.controller;

import com.aidan.api.apites.exceptions.TaskAlreadyExistsException;
import com.aidan.api.apites.exceptions.TaskNotFoundException;
import com.aidan.api.apites.tasks.model.Task;
import com.aidan.api.apites.tasks.model.TaskRequest;
import com.aidan.api.apites.tasks.model.TaskStatus;
import com.aidan.api.apites.tasks.service.TasksService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/tasks")
public class TaskController {

    private final TasksService tasksService;

    @GetMapping
    public List<Task> getAllTasks(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        return this.tasksService.getAllTasks(page, size).getContent();
    }

    @GetMapping("/{taskId}")
    public Task getTaskById(@PathVariable("taskId") UUID taskId) throws TaskNotFoundException {
        log.info("Retrieving all tasks with id %s".formatted(taskId));
        return this.tasksService.getTaskById(taskId);
    }

    @GetMapping("/name/{taskName}")
    public List<Task> getTaskByName(
            @PathVariable("taskName") String taskName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) throws TaskNotFoundException {
        log.info("Retrieving task with the name %s".formatted(taskName));
        return this.tasksService.getTaskByName(taskName, page, size).getContent();
    }

    @GetMapping("/status/{status}")
    public List<Task> getTasksByStatus(
            @PathVariable("status") TaskStatus status,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size
    ) {
        log.info("Retrieving all tasks with status %s".formatted(status));
        return this.tasksService.getTasksByStatus(status, page, size).getContent();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{taskId}")
    public void deleteTaskById(@PathVariable("taskId") UUID taskId) throws TaskNotFoundException {
        log.info("Deleting task with id %s".formatted(taskId));
        this.tasksService.deleteTaskById(taskId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping
    public void deleteAllTasks() {
        this.tasksService.deleteAllTasks();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/task")
    public void addNewTask(@Valid @RequestBody TaskRequest request, BindingResult result)
            throws TaskAlreadyExistsException {
        log.info("Adding new task %s".formatted(request.name()));
        this.tasksService.createNewTask(request);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PatchMapping("/{taskId}")
    public void updateTaskStatus(@PathVariable("taskId") UUID taskId, @RequestParam TaskStatus status)
            throws TaskNotFoundException {
        log.info("Updating task %s with status %s".formatted(taskId, status));
        this.tasksService.updateTaskStatus(taskId, status);
    }
}
