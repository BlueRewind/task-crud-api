package com.aidan.api.apites.tasks.service;

import com.aidan.api.apites.exceptions.TaskAlreadyExistsException;
import com.aidan.api.apites.exceptions.TaskNotFoundException;
import com.aidan.api.apites.exceptions.ValidationException;
import com.aidan.api.apites.tasks.model.Task;
import com.aidan.api.apites.tasks.model.TaskCategory;
import com.aidan.api.apites.tasks.model.TaskRequest;
import com.aidan.api.apites.tasks.model.TaskStatus;
import com.aidan.api.apites.tasks.repository.TaskEntity;
import com.aidan.api.apites.tasks.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDateTime;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;


@Slf4j
@Service
@Validated
@RequiredArgsConstructor
public class TasksService {

    private final TaskRepository taskRepository;
    private static final Set<TaskCategory> VALID_CATEGORIES = EnumSet.of(
            TaskCategory.TRAVEL, TaskCategory.EDUCATION, TaskCategory.ENTERTAINMENT,
            TaskCategory.FAMILY, TaskCategory.FINANCIAL, TaskCategory.PERSONAL,
            TaskCategory.SOCIAL, TaskCategory.WORK, TaskCategory.HEALTH
    );
    public Page<Task> getAllTasks(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<TaskEntity> pagedEntity = this.taskRepository.findAll(pageable);
        List<Task> taskList = transformPagedEntityToList(pagedEntity);
        return new PageImpl<>(taskList, pagedEntity.getPageable(), pagedEntity.getTotalElements());
    }

    public Task getTaskById(UUID id) throws TaskNotFoundException {
        log.info("Fetching task by ID: {}", id);
        var entity = this.taskRepository.findById(id).orElseThrow(
                () -> new TaskNotFoundException("Could not find task by id %s".formatted(id)));
        return transformEntityToTask(entity);
    }

    public Page<Task> getTaskByName(String name, int page, int size) throws TaskNotFoundException {
        Pageable pageable = PageRequest.of(page, size);
        Page<TaskEntity> pagedEntity = this.taskRepository.findByName(name, pageable).orElseThrow(
                () -> new TaskNotFoundException("Could not find task by name %s".formatted(name)));
        List<Task> taskList = transformPagedEntityToList(pagedEntity);

        return new PageImpl<>(taskList, pagedEntity.getPageable(), pagedEntity.getTotalElements());
    }

    public Page<Task> getTasksByStatus(TaskStatus status, int page, int size) {
        this.validateStatus(status);
        Pageable pageable = PageRequest.of(page, size);
        Page<TaskEntity> pagedEntity = this.taskRepository.getByStatus(status, pageable);
        List<Task> taskList = transformPagedEntityToList(pagedEntity);
        return new PageImpl<>(taskList, pagedEntity.getPageable(), pagedEntity.getTotalElements());

    }

    public Page<Task> getTaskByCategory(TaskCategory category, int page, int size) {
        this.validateCategory(category);
        Pageable pageable = PageRequest.of(page, size);
        Page<TaskEntity> pagedEntiy = this.taskRepository.getByCategory(category, pageable);
        List<Task> taskList = transformPagedEntityToList(pagedEntiy);
        return new PageImpl<>(taskList, pagedEntiy.getPageable(), pagedEntiy.getTotalElements());
    }

    public void deleteTaskById(UUID id) throws TaskNotFoundException {
        boolean taskExists = this.taskRepository.existsById(id);
        if (taskExists) {
            this.taskRepository.deleteById(id);
        } else {
            throw new TaskNotFoundException("Task with ID %s does not exists".formatted(id));
        }

    }

    public void deleteAllTasks() {
        this.taskRepository.deleteAll();
    }

    public void createNewTask(TaskRequest request) throws TaskAlreadyExistsException {
        log.info("Creating new task");
        if (this.taskRepository.existsById(request.id())) {
            throw new TaskAlreadyExistsException("Task with the ID %s already exists".formatted(request.id()));
        }

        this.validateStatus(request.status());
        //this.validateCategory(request.category());
        log.info(request.category().name());
        log.info(request.category().toString());

        TaskEntity task = TaskEntity.builder()
                .id(request.id())
                .name(request.name())
                .status(request.status())
                .category(request.category())
                .priority(request.priority())
                .dueDate(request.dueDate())
                .creationDate(LocalDateTime.now()).build();
        log.info("Built task, saving to repo");
        this.taskRepository.save(task);
    }

    public void updateTaskStatus(UUID taskId, TaskStatus status) throws TaskNotFoundException {
        TaskEntity existingTask = taskRepository.findById(taskId)
                .orElseThrow(() -> new TaskNotFoundException("Task with id %s not found".formatted(taskId)));

        this.taskRepository.updateStatusByStatus(taskId, status.toString());
    }

    private void validateStatus(TaskStatus newStatus) {
        if (!EnumSet.of(TaskStatus.CREATED, TaskStatus.IN_PROGRESS, TaskStatus.PAUSED, TaskStatus.FINISHED, TaskStatus.ON_HOLD)
                .contains(newStatus)) {
            String validStatusOptions = EnumSet.allOf(TaskStatus.class).toString();
            throw new ValidationException(List.of("Status of %s is not valid. Choose from (%s)".formatted(newStatus, validStatusOptions)));
        }
    }

    private void validateCategory(TaskCategory newCategory) {
        if (!VALID_CATEGORIES.contains(newCategory)) {
            String validCategoryOptions = VALID_CATEGORIES.stream()
                    .map(Enum::name)
                    .collect(Collectors.joining(", "));
            throw new ValidationException((List.of(String.format(
                    "Category of %s is not valid. Choose from (%s)", newCategory, validCategoryOptions))));
        }
    }

    private List<Task> transformPagedEntityToList(Page<TaskEntity> pagedEntity) {
        return pagedEntity.getContent().stream()
                .map(this::transformEntityToTask)
                .collect(Collectors.toList());
    }

    private Task transformEntityToTask(TaskEntity entity) {
        return Task.builder().id(entity.getId())
                .name(entity.getName())
                .status(entity.getStatus())
                .category(entity.getCategory())
                .priority(entity.getPriority())
                .dueDate(entity.getDueDate())
                .creationDate(entity.getCreationDate())
                .build();
    }
}
