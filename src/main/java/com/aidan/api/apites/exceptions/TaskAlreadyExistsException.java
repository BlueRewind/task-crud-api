package com.aidan.api.apites.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class TaskAlreadyExistsException extends Exception {
    public TaskAlreadyExistsException(String msg) {
        super(msg);
    }
}
