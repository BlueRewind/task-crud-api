package com.aidan.api.apites.exceptions;

import lombok.Getter;

import java.util.List;

@Getter
public class ValidationException extends RuntimeException {
    private final List<String> details;

    public ValidationException(List<String> details) {
        super(String.format("Validation error: %s", String.join(", ", details)));
        this.details = details;
    }

}
