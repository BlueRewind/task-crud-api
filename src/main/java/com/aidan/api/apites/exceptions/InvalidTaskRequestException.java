package com.aidan.api.apites.exceptions;

public class InvalidTaskRequestException extends Exception {
    public InvalidTaskRequestException(String msg) {
        super(msg);
    }
}
