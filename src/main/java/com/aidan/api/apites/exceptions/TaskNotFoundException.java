package com.aidan.api.apites.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TaskNotFoundException extends Exception {
    public TaskNotFoundException(String msg) {
        super(msg);
    }
}
