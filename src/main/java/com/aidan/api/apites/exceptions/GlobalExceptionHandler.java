package com.aidan.api.apites.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({
            InvalidStatusException.class, InvalidTaskRequestException.class})
    ProblemDetail handleInvalidRequestException(Exception exception) {
        return ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler({ValidationException.class})
    ProblemDetail handleValidationException(ValidationException exception) {
        ProblemDetail problemDetail = ProblemDetail.forStatusAndDetail(HttpStatus.BAD_REQUEST, exception.getMessage());
        problemDetail.setProperty("Validation Errors", exception.getDetails());
        return problemDetail;
    }

    @ExceptionHandler({TaskAlreadyExistsException.class})
    ProblemDetail handleTaskAlreadyExists(Exception exception) {
        return ProblemDetail.forStatusAndDetail(HttpStatus.CONFLICT, exception.getMessage());
    }

    @ExceptionHandler({TaskNotFoundException.class})
    ProblemDetail handleTaskNotFoundException(Exception exception) {
        return ProblemDetail.forStatusAndDetail(HttpStatus.NOT_FOUND, exception.getMessage());
    }
}
