package com.aidan.api.apites;

import com.aidan.api.apites.user.UserConfigProps;
import com.aidan.api.apites.user.repository.UserEntity;
import com.aidan.api.apites.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
@RequiredArgsConstructor
public class DatabaseLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserConfigProps userConfigProps;

    @Override
    public void run(String... args) {
        loadUsers();
    }

    private void loadUsers() {
        log.info("loading users");
        var doesUserExist = this.userRepository.findByUsername(this.userConfigProps.username()).isEmpty();
        log.info(String.valueOf(doesUserExist));
        if (this.userRepository.findByUsername(this.userConfigProps.username()).isEmpty()) {
            log.info("saving user to db");
            UserEntity user = UserEntity.builder()
                    .username(this.userConfigProps.username())
                    .password(this.passwordEncoder.encode(this.userConfigProps.password()))
                    .roles(Arrays.stream(this.userConfigProps.roles()).toList())
                    .build();
            this.userRepository.save(user);
            log.info("user saved");
        } else {
            log.info("I am not empty");
        }
    }
}
