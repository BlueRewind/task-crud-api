package com.aidan.api.apites.user;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("api.users")

public record UserConfigProps(
        String username,
        String password,
        String[] roles
) {
}
