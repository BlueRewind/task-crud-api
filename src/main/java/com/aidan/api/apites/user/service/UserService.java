package com.aidan.api.apites.user.service;

import com.aidan.api.apites.user.repository.UserEntity;
import com.aidan.api.apites.user.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public Optional<UserEntity> getUserByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }

    public Optional<UserEntity> getUserDetails(UUID userId) {
        return this.userRepository.findById(userId);
    }

    public List<UserEntity> getAllUsers() {
        return this.userRepository.findAll();
    }
}
