package com.aidan.api.apites.user.controller;

import com.aidan.api.apites.security.model.LoginRequest;
import com.aidan.api.apites.security.service.AuthenticationService;
import com.aidan.api.apites.user.repository.UserEntity;
import com.aidan.api.apites.user.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class UserController {

    private final AuthenticationService userLoginService;
    private final UserService userService;

    @ResponseStatus(code = HttpStatus.ACCEPTED)
    @GetMapping("/user/{userId}")
    public Optional<UserEntity> getUserDetails(
            @RequestParam("userId") UUID userId
    ) {
        return this.userService.getUserDetails(userId);
    }

    @GetMapping("/users")
    public List<UserEntity> getUsers() {
        return this.userService.getAllUsers();
    }
}
