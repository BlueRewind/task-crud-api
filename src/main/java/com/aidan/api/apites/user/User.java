package com.aidan.api.apites.user;

import java.util.List;

public record User(String username, List<String> roles) {
}
