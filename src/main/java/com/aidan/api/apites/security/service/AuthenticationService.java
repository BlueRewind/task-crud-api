package com.aidan.api.apites.security.service;

import com.aidan.api.apites.security.model.LoginRequest;
import com.aidan.api.apites.security.model.SimpleUserDetails;
import com.aidan.api.apites.user.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserService userService;
    private final String secret = "asdfSFS34wfsdfsdfSDSD32dfsddDDerQSNCK34SOWEK5354fdgdf4";
    private final Key hmacKey = new SecretKeySpec(Base64.getDecoder().decode(secret), SignatureAlgorithm.HS256.getJcaName());
    private Optional<String> currentLoggedInToken = Optional.empty();

    public boolean isSomeoneElseLoggedIn(String token) {
        return currentLoggedInToken.isPresent() && !currentLoggedInToken.get().equals(token);
    }

//    public Optional<UserDetails> loadUserByToken(String token) throws Exception {
//        try {
//            String username = Jwts.parserBuilder()
//                    .setSigningKey(hmacKey)
//                    .build()
//                    .parseClaimsJws(token)
//                    .getBody().getSubject();
//            if (this.isSomeoneElseLoggedIn(token)) {
//                throw new Exception(); // Someone else is logged in
//            }
//
//            this.currentLoggedInToken = Optional.of(token);
//            return Optional.ofNullable(this.loadUserByUsername(username));
//        } catch (Exception e) {
//            throw new Exception();
//        }
//    }
//
//    public String loginUser(LoginRequest loginRequest) throws Exception {
//        log.info("Grabbing token");
//        var token = this.userService.getUserByUsername(loginRequest.username())
//                .map(user -> {
//                    Instant now = Instant.now();
//                    return Jwts.builder()
//                            .signWith(hmacKey)
//                            .setSubject(loginRequest.username())
//                            .setId(UUID.randomUUID().toString())
//                            .claim("roles", user.getRoles())
//                            .setIssuedAt(Date.from(now))
//                            .setExpiration(
//                                    Date.from(now.plus(60, ChronoUnit.MINUTES)))
//                            .compact();
//                }).orElseThrow(() -> new BadCredentialsException(
//                        "User not found by the username %s".formatted(loginRequest.username())));
//        if (this.isSomeoneElseLoggedIn(token)) {
//            throw new Exception();
//        }
//
//        this.currentLoggedInToken = Optional.of(token);
//        return token;
//    }
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return this.userService.getUserByUsername(username).map(SimpleUserDetails::new)
//                .orElse(null);
//    }

    public void logout() {
        log.info("Token for user removed");
        this.currentLoggedInToken = Optional.empty();
    }

}
