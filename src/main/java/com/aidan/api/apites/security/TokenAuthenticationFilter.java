package com.aidan.api.apites.security;

import com.aidan.api.apites.security.service.AuthenticationService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class TokenAuthenticationFilter {

    private final AuthenticationService userLoginService;

//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
//            throws ServletException, IOException {
//        Optional<UserDetails> userDetails;
//        try {
//            userDetails = this.getAuthToken(request)
//                    .flatMap(token -> {
//                        try {
//                            return this.userLoginService.loadUserByToken(token);
//                        } catch (Exception e) {
//                            return Optional.empty();
//                        }
//                    });
//        } catch (Exception e) {
//            response.setStatus(HttpStatus.UNAUTHORIZED.value());
//            response.getOutputStream().print(e.getMessage());
//            response.flushBuffer();
//            return;
//        }
//
//        if (userDetails.isEmpty()) {
//            filterChain.doFilter(request, response);
//            return;
//        }
//
//        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails.get(), null, userDetails.get().getAuthorities());
//        SecurityContextHolder.getContext().setAuthentication(authToken);
//        filterChain.doFilter(request, response);
//    }

    private Optional<String> getAuthToken(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        if (token == null || !token.startsWith("Bearer ")) {
            return Optional.empty();
        }

        return Optional.of(token.substring(7));
    }
}
