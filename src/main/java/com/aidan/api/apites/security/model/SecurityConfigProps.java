package com.aidan.api.apites.security.model;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("api.security")
public record SecurityConfigProps(String[] unauthenticatedEndpoints) {
}
