package com.aidan.api.apites.security.model;

public record LoginRequest(
        String username,
        String password
) {
}
