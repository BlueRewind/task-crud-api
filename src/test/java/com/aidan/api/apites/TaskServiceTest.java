package com.aidan.api.apites;

import com.aidan.api.apites.exceptions.TaskAlreadyExistsException;
import com.aidan.api.apites.exceptions.TaskNotFoundException;
import com.aidan.api.apites.tasks.model.Task;
import com.aidan.api.apites.tasks.model.TaskCategory;
import com.aidan.api.apites.tasks.model.TaskRequest;
import com.aidan.api.apites.tasks.model.TaskStatus;
import com.aidan.api.apites.tasks.repository.TaskEntity;
import com.aidan.api.apites.tasks.repository.TaskRepository;
import com.aidan.api.apites.tasks.service.TasksService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
public class TaskServiceTest extends PostgresTestConfig {

    @Autowired
    TaskRepository taskRepository;


    @AfterEach
    void tearDown() {
        taskRepository.deleteAll();
    }
    @Test
    void shouldGetAllTasks() {
        List<TaskEntity> tasks = List.of(
                new TaskEntity(
                        UUID.randomUUID(),
                        "test",
                        TaskStatus.CREATED,
                        TaskCategory.FAMILY,
                        1,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                ),
                new TaskEntity(
                        UUID.randomUUID(),
                        "test1",
                        TaskStatus.FINISHED,
                        TaskCategory.FAMILY,
                        1,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                )
        );
        taskRepository.saveAll(tasks);

        TasksService underTest = new TasksService(taskRepository);
        var actual = underTest.getAllTasks(0, 10).getContent();
        List<Task> expected = tasks.stream()
                .map(entity -> new Task(entity.getId(), entity.getName(), entity.getStatus(), entity.getCategory(),
                     entity.getPriority(), entity.getDueDate(), entity.getCreationDate()))
                .toList();
        assertEquals(expected, actual);
    }
    @Test
    void shouldGetTaskByName() throws TaskNotFoundException {
        List<TaskEntity> tasks = List.of(new TaskEntity(
                UUID.randomUUID(),
                "test12",
                TaskStatus.IN_PROGRESS,
                TaskCategory.FAMILY,
                2,
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        ));
        taskRepository.saveAll(tasks);
        TasksService underTest = new TasksService(taskRepository);

        var actual = underTest.getTaskByName("test12", 0, 10).getContent();
        List<Task> expected = tasks.stream()
                .map(entity -> new Task(entity.getId(), entity.getName(),  entity.getStatus(), entity.getCategory(),
                        entity.getPriority(), entity.getDueDate(), entity.getCreationDate()))
                .toList();
        assertEquals(expected, actual);
    }

    @Test
    void shouldGetTaskByStatus() {
        TaskStatus status = TaskStatus.IN_PROGRESS;
        List<TaskEntity> tasks = List.of(
                new TaskEntity(
                        UUID.randomUUID(),
                        "test12",
                        status,
                        TaskCategory.FAMILY,
                        2,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                ),
                new TaskEntity(
                        UUID.randomUUID(),
                        "test12",
                        TaskStatus.CREATED,
                        TaskCategory.FAMILY,
                        2,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                )
        );
        taskRepository.saveAll(tasks);
        TasksService underTest = new TasksService(taskRepository);

        var actual = underTest.getTasksByStatus(status, 0, 10).getContent();
        List<Task> expected = tasks.stream()
                .map(entity -> new Task(entity.getId(), entity.getName(),  entity.getStatus(), entity.getCategory(),
                        entity.getPriority(), entity.getDueDate(), entity.getCreationDate()))
                .toList();
        assertEquals(1, actual.size());

    }

    @Test
    void shouldDeleteAllTasks() {
        List<TaskEntity> tasks = List.of(
                new TaskEntity(
                        UUID.randomUUID(),
                        "test",
                        TaskStatus.CREATED,
                        TaskCategory.FAMILY,
                        1,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                ),
                new TaskEntity(
                        UUID.randomUUID(),
                        "test1",
                        TaskStatus.FINISHED,
                        TaskCategory.FAMILY,
                        1,
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                        LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
                )
        );
        taskRepository.saveAll(tasks);

        TasksService underTest = new TasksService(taskRepository);
        underTest.deleteAllTasks();
        assertEquals(Collections.EMPTY_LIST, underTest.getAllTasks(0, 10).getContent());
    }

    @Test
    void shouldCreateNewTask() throws TaskAlreadyExistsException {
        TaskRequest request = new TaskRequest(
                UUID.randomUUID(),
                "name",
                TaskStatus.CREATED,
                TaskCategory.FAMILY,
                1,
                LocalDateTime.now()
        );
        TasksService underTest = new TasksService(taskRepository);
        underTest.createNewTask(request);
        assertEquals(1, underTest.getAllTasks(0, 10).getContent().size());
    }

    @Test
    void shouldAlreadyExistsWhenCreateTask() throws TaskAlreadyExistsException {
        UUID duplicateId = UUID.fromString("3d5eb80d-795b-4773-bdc4-172228025b9f");
        TaskRequest request = new TaskRequest(
                duplicateId,
                "name",
                TaskStatus.CREATED,
                TaskCategory.FAMILY,
                1,
                LocalDateTime.now()
        );
        log.info("Test ID {}", duplicateId);
        TasksService underTest = new TasksService(taskRepository);
        underTest.createNewTask(request);
        // https://hibernate.atlassian.net/browse/HHH-11490
        // assertThrows(TaskNotFoundException.class, () -> underTest.createNewTask(request));
    }

    @Test
    void shouldThrowExceptionWhenFindById() {
        TaskEntity task = new TaskEntity(
            UUID.randomUUID(),
            "test12",
            TaskStatus.IN_PROGRESS,
                TaskCategory.FAMILY,
            2,
            LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
            LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        );
        taskRepository.save(task);
        TasksService underTest = new TasksService(taskRepository);

        assertThrows(TaskNotFoundException.class, () -> underTest.getTaskById(UUID.randomUUID()));
    }

    @Test
    void shouldReturnEmptyListWhenTaskWithWrongName() throws TaskNotFoundException {
        TaskEntity task = new TaskEntity(
                UUID.randomUUID(),
                "TASK",
                TaskStatus.IN_PROGRESS,
                TaskCategory.FAMILY,
                2,
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        );
        taskRepository.save(task);
        TasksService underTest = new TasksService(taskRepository);
        var actual = underTest.getTaskByName("not_in_db", 0, 10).getContent();
        assertEquals(0, actual.size());
    }

    @Test
    void shouldThrowExceptionDeleteTaskById() {
        UUID id = UUID.randomUUID();
        TaskEntity task = new TaskEntity(
                id,
                "test12",
                TaskStatus.IN_PROGRESS,
                TaskCategory.FAMILY,
                2,
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS),
                LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        );
        taskRepository.save(task);
        TasksService underTest = new TasksService(taskRepository);
        assertThrows(TaskNotFoundException.class, () -> underTest.deleteTaskById(UUID.randomUUID()));
    }
}
