# Use the official OpenJDK image as the base image
FROM openjdk:17-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the JAR file into the container
COPY target/api-tes-1.0.0.jar /app/api.jar

# Expose the port that the Spring Boot application will run on
EXPOSE 8888

# Command to run the application
CMD ["java", "-Dserver.port=8888", "-jar", "api.jar"]
